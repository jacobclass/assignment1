﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6211Assignment01
{
    class Program
    {
        static string input;
        static bool exit;
        static int selection;

        static void Main(string[] args)
        {
            exit = false;
            //loop main menu until selection 5 is made
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("/~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("|                            Main Menu                            |");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("|         1: queue menu                                           |");
                Console.WriteLine("|         2: palindrome checker                                   |");
                Console.WriteLine("|         3: convert infix to postfix                             |");
                Console.WriteLine("|         4: evaluate postfix equation                            |");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("|         5: quit to menu                                         |");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("\\~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~/");
                Console.WriteLine();
                Console.Write("Please enter selection:");

                input = Console.ReadLine();

                //prevent crashing on invalid input and allow reselection
                while (!Int32.TryParse(input, out selection))
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid input, please try again");
                    Console.Write("Please enter selection:");
                    input = Console.ReadLine();
                }

                Console.Clear();
                //checks the selection to go to whats requested
                switch (selection)
                {
                    case 1:
                        Question1.menu();
                        break;

                    case 2:
                        Question2.checker();
                        Console.ReadKey();
                        break;

                    case 3:
                        Question3.converter();
                        Console.ReadKey();
                        break;

                    case 4:
                        Question4.EvaluatePostfixExpression();
                        Console.ReadKey();
                        break;

                    case 5:
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("Invalid number, please try again");
                        Console.ReadKey();
                        break;
                }

            }
        }
    }

    public static class Question1
    {
        static Queue<string> Queue1 = new Queue<string>();
        static Array Array1;

        static string input;
        static int selection;
        static string check;
        static bool exit;

        public static void menu()
        {
            exit = false;
            //loops through menu until option 6 is selected
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("/~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("|                           Queue Menu                            |");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("|         1: print queue                                          |");
                Console.WriteLine("|         2: add to queue                                         |");
                Console.WriteLine("|         3: remove from queue                                    |");
                Console.WriteLine("|         4: check if queue contains item                         |");
                Console.WriteLine("|         5: convert queue to array and print contents            |");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("|         6: quit to menu                                         |");
                Console.WriteLine("|                                                                 |");
                Console.WriteLine("\\~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~/");
                Console.WriteLine();
                Console.Write("Please enter selection:");

                input = Console.ReadLine();

                //prevents invalid input and allows reselection
                while (!Int32.TryParse(input, out selection))
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid input, please try again");
                    Console.Write("Please enter selection:");
                    input = Console.ReadLine();
                }

                Console.Clear();

                switch (selection)
                {
                    case 1:
                        printQueue();
                        Console.ReadKey();
                        break;

                    case 2:
                        Console.Write("please enter item to add:");
                        input = Console.ReadLine();
                        enqueue(input);
                        Console.ReadKey();
                        break;

                    case 3:
                        dequeue();
                        Console.ReadKey();
                        break;

                    case 4:
                        Console.Write("please enter item to check for:");
                        input = Console.ReadLine();
                        Contains(input);
                        Console.ReadKey();
                        break;

                    case 5:
                        ToArray();
                        Console.ReadKey();
                        break;

                    case 6:
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("Invalid number, please try again");
                        Console.ReadKey();
                        break;
                }
            }
        }

        //prints queue to console
        static void printQueue()
        {
            Console.WriteLine("The queue currently contains:");
            Console.WriteLine();
            foreach (string x in Queue1)
            {
                Console.WriteLine($"- {x}");
            }
        }

        //adds supplied input to queue
        static void enqueue(string input)
        {
            Queue1.Enqueue(input);
            Console.WriteLine();
            Console.WriteLine($"{input} has been added to the queue");
        }

        //removes object from beggining of queue
        static void dequeue()
        {
            //prevents trying to remove from queue when nothing in queue
            if (Queue1.Count > 0)
            {
                check = Queue1.Dequeue();
                Console.WriteLine($"{check} has been dequeued from the queue");
            }
            else
            {
                Console.WriteLine("Queue is already empty");
            }
        }

        //checks if queue contains an item
        static void Contains(string item)
        {
            if (Queue1.Contains(item))
            {
                Console.WriteLine($"Queue contains {item}");
            }
            else
            {
                Console.WriteLine($"Queue does not contain {item}");
            }
        }

        //converts queue to array and then prints the array
        static void ToArray()
        {
            //prevents converting to array when nothing is in the queue
            if (Queue1.Count > 0)
            {
                Array1 = Queue1.ToArray();
                Console.WriteLine("Queue has been converted to an array");
                Console.WriteLine();
                Console.WriteLine("Array contents:");
                Console.WriteLine();
                foreach (var x in Array1)
                {
                    Console.WriteLine($"- {x}");
                }
            }
            else
            {
                Console.WriteLine("No items in queue to convert to array");
            }
        }
    }

    public static class Question2
    {
        static string input;
        static string check;
        static Stack<Char> palinStack = new Stack<Char>();

        public static void checker()
        {
            //resets variables in case of previous use to avoid problems
            palinStack.Clear();
            check = null;

            //takes input then removes spaces and creates case insensitivity
            Console.Write("Please enter word: ");
            input = Console.ReadLine().ToLower();
            input = input.Replace(" ", "");
            foreach (char c in input)
            {
                //ignores punctuation
                if (Char.IsPunctuation(c))
                {
                    input = input.Replace(c.ToString(), "");
                }
                else
                {
                    //puts any letters in the stack
                    palinStack.Push(c);
                }
            }
            foreach (char c in palinStack)
            {
                //puts all letters from stack into new string to create reverse of string
                check += c;
            }
            //if input is the same as the reversed string it is a palindrome
            if (check == input)
            {
                Console.WriteLine($"{input} is a palindrome");
            }
            else
            {
                Console.WriteLine($"{input} is not a palindrome");
            }

        }
    }

    public static class Question3
    {
        static Stack<char> opStack = new Stack<char>();
        static string input;
        static string postfix;
        static char ch;
        static int left;
        static int right;
        static int prio;

        //takes input for infix, sends to check parenthesis then sends for conversion
        public static void converter()
        {
            //reset variables in case they have been set before 
            opStack.Clear();
            postfix = "";
            Console.Write("please enter infix equation to convert:");
            input = Console.ReadLine();
            //removes spaces on input
            input.Replace(" ", "");
            //if all parenthesis have partners, passes equation to the converter
            if (checkParenthesis(input))
            {
                Console.WriteLine(convert(input));
            }
            else
            {
                Console.WriteLine("parenthesis unbalanced, equation invalid");
            }
        }

        static string convert(string infix)
        {
            //adds parenthesis to know when equation starts/ends
            opStack.Push('(');
            infix += ')';
            
            //goes through all infix characters to create postfix equation
            for (int i = 0; i < infix.Length; i++)
            {
                ch = infix[i];
                //adds numbers to string
                if (char.IsDigit(ch)) 
                {
                    postfix += ch;
                }
                else if (ch == '(')
                {
                    opStack.Push(ch);
                }
                else if (isOperator(ch))
                {
                    //checks precedence and adds operators accordingly
                    prio = precedence(ch);
                    while(precedence(opStack.Peek()) >= prio)
                    {
                        postfix += opStack.Pop();
                    }
                    opStack.Push(ch);
                }
                else if (ch == ')')
                {
                    //if character is parenthesis, pops stack until partner parenthesis found
                    while(opStack.Peek() != '(')
                    {
                        postfix += opStack.Pop();
                    }
                    //ignores partner parenthesis
                    opStack.Pop();
                }
            }
            return (postfix);
        }

        //determines the precedence of an operator and returns related int (higher int, higher precedence)
        static int precedence(char c)
        {
            if(c == '^')
            {
                return (3);
            }
            else if ("*/%".Contains(c))
            {
                return (2);
            }
            else if ("+-".Contains(c))
            {
                return (1);
            }
            else
            {
                return (0);
            }
        }

        //checks that there is all parenthesis have pairs to avoid problems
        static bool checkParenthesis(string test)
        {
            right = 0;
            left = 0;
            foreach (char c in test)
            {
                if (c == '(')
                {
                    left++;
                }
                else if (c == ')')
                {
                    right++;
                }
            }
            if (left == right)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        //checks if character is a valid operator 
        static bool isOperator(char c)
        {
            if ("^/%*+-".Contains(c))
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }
    }

    public static class Question4
    {
        static string input;
        static int answer;
        static Stack<string> stack = new Stack<string>();
        static int num1;
        static int num2;
        static bool invalid = false;

        //takes user input  for evaluation (used if no string is  present)
        public static void EvaluatePostfixExpression()
        {
            Console.Write("Please enter postfix equation:");
            input = Console.ReadLine();
            EvaluatePostfixExpression(input);
        }

        //evauates postfix expression (used when ones supplied)
        public static void EvaluatePostfixExpression(string postfix)
        {
            //reset variables in case previously set
            invalid = false;
            stack.Clear();
            Console.WriteLine();
            foreach(char c in postfix)
            {
                //checks if characters a number and adds to stack if it is
                if (char.IsNumber(c))
                {
                    stack.Push(c.ToString());
                }
                else
                {
                    //checks there is enough in the stack
                    if (stack.Count > 1)
                    {
                        //put into numbers (reverse order due to stack)
                        num2 = Int32.Parse(stack.Pop());
                        num1 = Int32.Parse(stack.Pop());
                        stack.Push(calculate(num1, num2, c).ToString());
                    }
                    else
                    {
                        //not enough numbers for equation so declare invalid
                        Console.WriteLine("Invalid postfix expression");
                        invalid = true;
                    }
                }
                if (invalid)
                {
                    break;
                }
            }

            //if excess numbers (too many numbers per operator declare invalid)
            if (stack.Count != 1)
            {
                invalid = true;
                Console.WriteLine("invalid postfix expression");
            }

            //writes answer if postfix expression determined to be valid
            if (!invalid)
            {
                Console.WriteLine($"{postfix} = {stack.Pop()}");
            }
        }

        //checks what operator is used and calculates the answer
        static int calculate(int a, int b, char op)
        {
            switch (op)
            {
                case '+':
                    answer = a + b;
                    break;

                case '-':
                    answer = a - b;
                    break;

                case '*':
                    answer = a * b;
                    break;

                case '/':
                    answer = a / b;
                    break;

                case '%':
                    answer = a % b;
                    break;

                case '^':
                    answer = (int)Math.Pow(a, b);
                    break;

                //if not valid as operator declare invalid equation
                default:
                    Console.WriteLine("Invalid postfix expression");
                    invalid = true;
                    break;
            }

            return (answer);
        }
    }
}